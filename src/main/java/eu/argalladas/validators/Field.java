package eu.argalladas.validators;

/**
 * Copyright 2018 (C) argalladas.eu
 * Created on: 14/04/2018
 * Author: fernando.blanco
 **/
import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;


@Target({FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, LOCAL_VARIABLE})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = {FieldValidator.class})

public @interface Field {

    String message();

    String[] fields() default {};

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
