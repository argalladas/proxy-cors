package eu.argalladas.validators;

/**
 * Copyright 2018 (C) argalladas.eu
 * Created on: 14/04/2018
 * Author: fernando.blanco
 **/
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.argalladas.exception.ApiException;
import eu.argalladas.exception.PreconditionError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.NotNull;

@Component
public class FieldValidator implements ConstraintValidator<Field, Object> {
    private static final Logger LOGGER = LoggerFactory.getLogger(FieldValidator.class);
    private static final Integer PARENT_FIELD = 0;
    private static final Integer CHILDREN_FIELD = 1;
    private Field field;

    private static void validate(@NotNull Object object, @NotNull String... fields) throws ApiException {
        for (String fieldName : fields) {
            validate(object, fieldName);
        }
    }

    private static void validate(@NotNull Object object, @NotNull String fieldName) throws ApiException {
        try {
            String[] partsFieldName = fieldName.replaceFirst("\\.", "\\$").split("\\$");
            boolean complexField = partsFieldName.length > 1;

            fieldName = complexField ? partsFieldName[PARENT_FIELD] : fieldName;

            java.lang.reflect.Field field = object.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            if (field.get(object) == null) {
                JsonProperty annotation = field.getAnnotation(JsonProperty.class);
                String externalName = annotation == null ? fieldName : annotation.value();
                LOGGER.error(String.format("Require param %s [%s] in object %s", fieldName, externalName, object.getClass()));
                throw new PreconditionError(externalName);
            } else if (complexField) {
                validate(field.get(object), partsFieldName[CHILDREN_FIELD]);
            }

        } catch (NoSuchFieldException ex) {
            throw new PreconditionError(String.format("Not defined param %s", fieldName));
        } catch (IllegalAccessException ex) {
            throw new PreconditionError(String.format("Can not access param %s", fieldName));
        }
    }

    @Override
    public void initialize(Field constraintAnnotation) {
        this.field = constraintAnnotation;
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        try {
            validate(value, field.fields());
            return true;
        } catch (ApiException e) {
            return false;
        }
    }

}