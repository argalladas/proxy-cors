package eu.argalladas.services;

/**
 * Copyright 2018 (C) argalladas.eu
 * Created on: 14/04/2018
 * Author: fernando.blanco
 **/
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.GetRequest;
import com.mashape.unirest.request.HttpRequestWithBody;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.util.HashMap;

@Component
public class RequestService {

    public RequestService() {
        Unirest.setObjectMapper(new ObjectMapper() {
            private com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper
                    = new com.fasterxml.jackson.databind.ObjectMapper();

            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return jacksonObjectMapper.readValue(value, valueType);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            public String writeValue(Object value) {
                try {
                    return jacksonObjectMapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    private static MultivaluedMap<String, String> cleanHeader(MultivaluedMap<String, String> headers) {
        headers.remove(HttpHeaders.HOST);
        headers.remove(HttpHeaders.CONTENT_LENGTH);
        return headers;
    }

    public GetRequest get(@NotNull(message = "url") String url, @NotNull(message = "headers") HttpHeaders headers) {
        GetRequest request = Unirest.get(url);
        request.headers(new HashMap<>());
        MultivaluedMap<String, String> cleanHeaders = this.cleanHeader(headers.getRequestHeaders());
        for (String key : cleanHeaders.keySet()) {
            request.header(key, headers.getHeaderString(key));
        }
        return request;
    }

    public HttpRequestWithBody post(@NotNull(message = "url") String url, @NotNull(message = "headers") HttpHeaders headers, Object body) {
        return this.request(Unirest.post(url), headers, body);
    }

    public HttpRequestWithBody put(@NotNull(message = "url") String url, @NotNull(message = "headers") HttpHeaders headers, Object body) {
        return this.request(Unirest.put(url), headers, body);
    }

    public HttpRequestWithBody delete(@NotNull(message = "url") String url, @NotNull(message = "headers") HttpHeaders headers, Object body) {
        return this.request(Unirest.delete(url), headers, body);
    }

    private HttpRequestWithBody request(@NotNull(message = "request") HttpRequestWithBody request, @NotNull(message = "headers") HttpHeaders headers, Object body) {
        request.headers(new HashMap<>());
        MultivaluedMap<String, String> cleanHeaders = this.cleanHeader(headers.getRequestHeaders());
        for (String key : cleanHeaders.keySet()) {
            request.header(key, headers.getHeaderString(key));
        }
        request.body(body);
        return request;
    }
}
