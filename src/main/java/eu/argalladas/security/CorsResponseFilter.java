package eu.argalladas.security;

/**
 * Copyright 2018 (C) argalladas.eu
 * Created on: 14/04/2018
 * Author: fernando.blanco
 **/
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Component
public class CorsResponseFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse responsehttp = ((HttpServletResponse) response);

        responsehttp.addHeader("Access-Control-Allow-Origin", "*");
        responsehttp.addHeader("Access-Control-Allow-Methods", "*");
        responsehttp.addHeader("Access-Control-Allow-Headers", "*");
        responsehttp.addHeader("Access-Control-Expose-Headers", "*");
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }

}