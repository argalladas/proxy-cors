package eu.argalladas.exception;

/**
 * Copyright 2018 (C) argalladas.eu
 * Created on: 14/04/2018
 * Author: fernando.blanco
 **/
public abstract class ApiException extends Exception {
    public ApiException(String msg) {
        super(msg);
    }

    public abstract Integer getCode();

}
