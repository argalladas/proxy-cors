package eu.argalladas.exception;

/**
 * Copyright 2018 (C) argalladas.eu
 * Created on: 14/04/2018
 * Author: fernando.blanco
 **/
import javax.servlet.http.HttpServletResponse;

public class PreconditionError extends ApiException {
    public PreconditionError() {
        this("Precondition faild");
    }

    public PreconditionError(String param) {
        super(String.format("Necesary param %s", param));
    }

    public PreconditionError(String msg, String param) {
        super(String.format("%s %s", msg, param));
    }

    public Integer getCode() {
        return HttpServletResponse.SC_PRECONDITION_FAILED;
    }

}