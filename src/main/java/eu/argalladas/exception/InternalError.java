package eu.argalladas.exception;

/**
 * Copyright 2018 (C) argalladas.eu
 * Created on: 14/04/2018
 * Author: fernando.blanco
 **/
import javax.servlet.http.HttpServletResponse;

public class InternalError extends ApiException {

    public InternalError(String param) {
        super("Internal error: " + param);
    }

    public Integer getCode() {
        return HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
    }

}