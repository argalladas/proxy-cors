package eu.argalladas.exception.logic;

/**
 * Copyright 2018 (C) argalladas.eu
 * Created on: 14/04/2018
 * Author: fernando.blanco
 **/
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Erro {
    private String msg;
    private String code;


    public Erro(Integer code, String msg) {
        this.msg = msg;
        this.code = String.format("ERR_%s", code);
    }


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
