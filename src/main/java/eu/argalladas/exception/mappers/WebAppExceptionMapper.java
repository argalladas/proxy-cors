package eu.argalladas.exception.mappers;

/**
 * Copyright 2018 (C) argalladas.eu
 * Created on: 14/04/2018
 * Author: fernando.blanco
 **/
import eu.argalladas.exception.logic.Erro;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;


@Provider
public class WebAppExceptionMapper implements ExceptionMapper<Exception> {
    private static final Logger LOGGER = LoggerFactory.getLogger(WebAppExceptionMapper.class);

    @Override
    public Response toResponse(Exception exception) {
        LOGGER.error(String.format("%s %s", exception.getStackTrace(), exception.getMessage()));
        Integer code = (exception instanceof WebApplicationException) ? ((WebApplicationException) exception).getResponse().getStatus() : HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

        return Response
                .status(code)
                .entity(new Erro(code, exception.getMessage()))
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}