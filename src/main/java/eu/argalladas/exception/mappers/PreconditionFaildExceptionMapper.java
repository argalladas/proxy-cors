package eu.argalladas.exception.mappers;

/**
 * Copyright 2018 (C) argalladas.eu
 * Created on: 14/04/2018
 * Author: fernando.blanco
 **/
import eu.argalladas.exception.logic.Erro;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.List;
import java.util.stream.Collectors;


@Provider
public class PreconditionFaildExceptionMapper implements ExceptionMapper<javax.validation.ConstraintViolationException> {
    private static final Logger LOGGER = LoggerFactory.getLogger(PreconditionFaildExceptionMapper.class);

    @Override
    public Response toResponse(javax.validation.ConstraintViolationException exception) {
        LOGGER.error(String.format("%s %s", exception.getStackTrace(), exception.getMessage()));
        List<String> elementsRequired = exception.getConstraintViolations().stream().map(x -> x.getMessage()).collect(Collectors.toList());
        String msg = String.format("Required params: %s", String.join(",", elementsRequired));
        return Response
                .status(HttpServletResponse.SC_PRECONDITION_FAILED)
                .entity(new Erro(HttpServletResponse.SC_PRECONDITION_FAILED, msg))
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

}