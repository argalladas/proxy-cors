package eu.argalladas.exception.mappers;

/**
 * Copyright 2018 (C) argalladas.eu
 * Created on: 14/04/2018
 * Author: fernando.blanco
 **/
import eu.argalladas.exception.ApiException;
import eu.argalladas.exception.logic.Erro;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ApiExceptionMapper implements ExceptionMapper<ApiException> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApiExceptionMapper.class);

    @Override
    public Response toResponse(ApiException exception) {
        LOGGER.error(String.format("%s %s", exception.getStackTrace(), exception.getMessage()));
        return Response
                .status(exception.getCode())
                .entity(new Erro(exception.getCode(), exception.getMessage()))
                .type(MediaType.APPLICATION_JSON)
                .build();
    }
}