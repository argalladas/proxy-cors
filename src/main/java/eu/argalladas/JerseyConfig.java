package eu.argalladas;

/**
 * Copyright 2018 (C) argalladas.eu
 * Created on: 14/04/2018
 * Author: fernando.blanco
 **/
import eu.argalladas.api.Proxy;
import eu.argalladas.exception.mappers.ApiExceptionMapper;
import eu.argalladas.exception.mappers.PreconditionFaildExceptionMapper;
import eu.argalladas.exception.mappers.WebAppExceptionMapper;
import eu.argalladas.security.CorsResponseFilter;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        registerEndpoints();
        registerMappers();
        registerFilters();
    }

    private void registerEndpoints() {
        register(Proxy.class);
    }

    private void registerMappers() {
        register(PreconditionFaildExceptionMapper.class);
        register(WebAppExceptionMapper.class);
        register(ApiExceptionMapper.class);

    }

    private void registerFilters() {
        register(CorsResponseFilter.class);
    }
}