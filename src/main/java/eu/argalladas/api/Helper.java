package eu.argalladas.api;

/**
 * Copyright 2018 (C) argalladas.eu
 * Created on: 14/04/2018
 * Author: fernando.blanco
 **/
import com.mashape.unirest.http.Headers;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;

@Component
public class Helper {

    public Helper() {
    }

    public Response generateResponse(HttpResponse httpResponse) {
        Response response = Response.ok().build();
        if (httpResponse.getBody() instanceof String) {
            response = Response.status(httpResponse.getStatus()).entity(httpResponse.getBody()).build();
        } else if (httpResponse.getBody() instanceof JsonNode) {
            if (((JsonNode) httpResponse.getBody()).isArray()) {
                response = Response.status(httpResponse.getStatus()).entity(((JsonNode) httpResponse.getBody()).getArray().toString()).build();
            } else {
                response = Response.status(httpResponse.getStatus()).entity(((JsonNode) httpResponse.getBody()).getObject().toString()).build();
            }
        }

        return addHeaders(response, httpResponse.getHeaders());
    }

    private Response addHeaders(Response response, Headers headers) {
        headers.remove("X-Frame-Options");
        headers.remove("X-Content-Type-Options");
        headers.remove("X-Xss-Protection");
        headers.remove("Access-Control-Allow-Origin");
        headers.remove("Access-Control-Allow-Methods");
        headers.remove("Access-Control-Allow-Headers");
        headers.remove("Access-Control-Expose-Headers");
        headers.remove("Content-Type");
        for (Object key : headers.keySet()) {
            response.getHeaders().add(String.valueOf(key), String.valueOf(headers.get(key)));
        }
        return response;
    }
}
