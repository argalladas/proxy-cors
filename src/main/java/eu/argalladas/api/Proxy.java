package eu.argalladas.api;

/**
 * Copyright 2018 (C) argalladas.eu
 * Created on: 14/04/2018
 * Author: fernando.blanco
 **/
import eu.argalladas.services.RequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("/proxy")
public class Proxy {
    private static final Logger LOGGER = LoggerFactory.getLogger(Proxy.class);

    @Autowired
    RequestService requestService;

    @Autowired
    Helper helper;

    @GET
    public Response getRequest(@Context HttpHeaders headers, @QueryParam("url") String url, @RequestBody Object body) throws Exception {
        if (headers.getRequestHeader(HttpHeaders.CONTENT_TYPE) == null || headers.getRequestHeader(HttpHeaders.CONTENT_TYPE).indexOf(MediaType.APPLICATION_JSON) == -1) {
            return helper.generateResponse(requestService.get(url, headers).asString());
        } else {
            return helper.generateResponse(requestService.get(url, headers).asJson());
        }
    }


    @POST
    public Response postRequest(@Context HttpHeaders headers, @QueryParam("url") String url, @RequestBody Object body) throws Exception {
        if (headers.getRequestHeader(HttpHeaders.CONTENT_TYPE) == null || headers.getRequestHeader(HttpHeaders.CONTENT_TYPE).indexOf(MediaType.APPLICATION_JSON) == -1) {
            return helper.generateResponse(requestService.post(url, headers, body).asString());
        } else {
            return helper.generateResponse(requestService.post(url, headers, body).asJson());
        }
    }


    @PUT
    public Response putRequest(@Context HttpHeaders headers, @QueryParam("url") String url, @RequestBody Object body) throws Exception {
        if (headers.getRequestHeader(HttpHeaders.CONTENT_TYPE).equals(MediaType.APPLICATION_JSON)) {
            return helper.generateResponse(requestService.put(url, headers, body).asJson());
        } else {
            return helper.generateResponse(requestService.put(url, headers, body).asString());
        }
    }


    @DELETE
    public Response deleteRequest(@Context HttpHeaders headers, @QueryParam("url") String url, @RequestBody Object body) throws Exception {
        if (headers.getRequestHeader(HttpHeaders.CONTENT_TYPE).equals(MediaType.APPLICATION_JSON)) {
            return helper.generateResponse(requestService.delete(url, headers, body).asJson());
        } else {
            return helper.generateResponse(requestService.delete(url, headers, body).asString());
        }
    }

}